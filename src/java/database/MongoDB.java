package database;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author Davi
 */
public class MongoDB {

    private static final String DATABASE_NAME = "mydb";
    private static MongoDB instance;
    
    private MongoClient mongoClient;

    public MongoDB() {
       mongoClient = new MongoClient();
    }
    
    public static MongoDB getInstance(){
        if (instance == null){
            instance = new MongoDB();
        }
        return instance;
    }
    
    public MongoDatabase getDatabase(){
        return mongoClient.getDatabase(DATABASE_NAME);
    }
    
}
