package services;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import database.MongoDB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Service to remove a planet from database
 * @author Davi
 */
@Path("/remove-planet")
public class RemovePlanet {
    
    @Path("/name/{name}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String removeByName(@PathParam("name") String name) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", name);
        
       return removePlanet(searchQuery);
    }

    @Path("/id/{id}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String removeById(@PathParam("id") String id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("_id", new ObjectId(id));

        return removePlanet(searchQuery);
    }

    private String removePlanet(BasicDBObject searchQuery) {
        DeleteResult deleteResult = null;
        try {
            MongoCollection<Document> collection = MongoDB.getInstance().getDatabase().getCollection("planets");
            deleteResult = collection.deleteOne(searchQuery);

        } catch (MongoException me) {
            me.printStackTrace();
            return "{success : " + false + "}";
        }
        if (deleteResult == null) {
            return "{success : " + false + "}";
        } else {
            return "{success : " + (deleteResult.getDeletedCount() > 0) + "}";
        }
    }
}
