package services;

import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import database.MongoDB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.bson.Document;

/**
 * Service to insert a new planet in database
 *
 * @author Davi
 */
@Path("/insert-planet")
public class InsertPlanet {

    @Path("/{name}/{climate}/{terrain}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String insertNewPlanet(@PathParam("name") String name, @PathParam("climate") String climate, @PathParam("terrain") String terrain) {
        
        boolean success = false;
        try {
            MongoCollection<Document> collection = MongoDB.getInstance().getDatabase().getCollection("planets");
            Document doc = new Document("name", name)
                    .append("climate", climate)
                    .append("terrain", terrain);
            collection.insertOne(doc);
            
        } catch (MongoException me) {
            me.printStackTrace();
            return "{success : " + success + "}";
        }
        success = true;
        return "{success : " + success + "}";
    }
}
