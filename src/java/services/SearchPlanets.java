package services;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import database.MongoDB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Service to search planets from database
 *
 * @author Davi
 */
@Path("/search-planets")
public class SearchPlanets {

    @Path("/name/{name}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String searchByName(@PathParam("name") String name) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", name);

        return searchPlanet(searchQuery);
    }

    @Path("/id/{id}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String searchById(@PathParam("id") String id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("_id", new ObjectId(id));

        return searchPlanet(searchQuery);
    }

    private String searchPlanet(BasicDBObject searchQuery) {
        Document doc = null;
        try {
            MongoCollection<Document> collection = MongoDB.getInstance().getDatabase().getCollection("planets");
            doc = collection.find(searchQuery).first();

        } catch (MongoException me) {
            me.printStackTrace();
            return "{success : " + false + "}";
        }
        if (doc == null) {
            return "{}";
        } else {
            return doc.toJson();
        }
    }

    @Path("/all/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String searchAll() {
        MongoCollection<Document> collection = MongoDB.getInstance().getDatabase().getCollection("planets");

        StringBuilder sb = new StringBuilder();
        sb.append("{ planets: [");
        for (Document cur : collection.find()) {
            sb.append(cur.toJson());
            sb.append(",");
        }
        sb.append("] }");

        return sb.toString();
    }
}
