# sw-rest

Projeto Desafio Técnico - Dev. Back-End. O projeto é uma API REST em JavaEE (Glassfish) utilizando o MongoDB capaz de inserir, listar e remover planetas através da url do serviço.

**Inserir planeta:**

`http://localhost:[porta-glassfish]/sw-rest/insert-planet/[nome-planeta]/[clima]/[terreno]`

**Listar todos planetas existentes:**

`http://localhost:[porta-glassfish]/sw-rest/search-planets/all`

**Buscar planeta pelo nome:**

`http://localhost:[porta-glassfish]/sw-rest/search-planets/name/[nome]`

**Buscar planeta pelo id:**

`http://localhost:[porta-glassfish]/sw-rest/search-planets/id/[id]`

**Remover planeta pelo nome:**

`http://localhost:[porta-glassfish]/sw-rest/remove-planet/name/[nome]`

**Remover planeta pelo id:**

`http://localhost:[porta-glassfish]/sw-rest/remove-planet/id/[id]`